import React,{useState,useMemo} from 'react';

export const HookApp = () => {
    const [name, setName] = useState("Mauricio");
    const [number, setNumber] = useState(5);
    
    return (
        <div className="App">
        <input value={name} onChange={(e) => setName(e.target.value)} />
        <NameDisplay name={name} />
        <hr />
        <input value={number} onChange={(e) => setNumber(e.target.value)} />
        <ExponentDisplay number={number} />
        </div>
    );
}

/* function NameDisplay({ name }) {
  console.log("==> Rerendering NameDisplay...");
  return <p>Your name is {name}</p>;
}
 */
const NameDisplay = React.memo(function({ name }) {
    console.log("Rerendering NameDisplay...");
    return <p>Your name is {name}</p>;
  });
  
 function ExponentDisplay({ number }) {
  console.log("==> Rerendering ExponentDisplay...");

  /* let result = Number(number);
    for (let x = 1; x < 10; x++) {
        result = result * number;
    } */
    const result=useMemo(() => {
        let r=number;
        console.log("==>Calculando...",r);
        for (let x = 0; x < 10; x++) {
            r=r*number;
        }
        return r;
    }, [number]);

    return (
        <p>
        ({number})^10 = {result}
        </p>
    );
}
