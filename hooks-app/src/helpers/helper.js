const noEmptyFields = fields => {
    for (const item in fields) {
      const value = fields[item];
  
      if (value.trim().length < 1) {
        return false;
      }
    }
  
    return true;
  };
  
  const update = (state, { data, loading }) => {
    return {
      ...state,
      status: "VERIFYING",
      error: null,
      response: null,
      message: null,
      loading
    };
  };
  
  const success = (state, { data, loading }) => {
    return {
      ...state,
      status: "READY",
      message: data.message,
      response: data,
      loading
    };
  };
  
  const error = (state, { data, loading }) => {
    return {
      ...state,
      status: "ERROR",
      message: data.message,
      response: null,
      loading
    };
  };
  
  export { noEmptyFields, update, success, error };
  