import React from 'react';
import {useCounter} from '../../hooks/useCounter';

import './counter.css';

export const CounterWithCustomHook = () => {
    
    const {state:counter,increment,decrement,reset}=useCounter(20);
    
    return (
        <>
          <h1>Counter with hook {counter}</h1>
          <hr/>

          <button onClick={()=>increment(2)} className="btn btn-primary">+1</button>
          <button onClick={reset} className="btn btn-warning">Reset</button> 
          <button onClick={()=>decrement(4)} className="btn btn-danger">-1</button>   
        </>
    )
}
