import React, { useReducer } from 'react';

const reducerCounter=(state,action)=>{
  let newState={...state};

  switch (action.type) {
    case "INCREMENT":
        if (!state.frozen) newState.count=state.count + 1; 
      break;
    case "DECREMENT":
        if (!state.frozen) newState.count=state.count - 1; 
      break;
    case "RESET":
        if (!state.frozen) newState.count=0; 
      break;
    case "FROZEN":
        newState.frozen=!state.frozen; 
      break;
    default:
      break;
  }

  return newState;
}

function App() {
  const initialState={
    count:0,
    forzen:false
  };

  const [counter, dispatchCounter] = useReducer(reducerCounter, initialState)

  return (
    <div>
      <p>Count:  {counter.count}</p>
      <button onClick={(e)=>{dispatchCounter({type:"INCREMENT"})}}>INCREMENT</button>
      <button onClick={(e)=>{dispatchCounter({type:"DECREMENT"})}}>DECREMENT</button>
      <button onClick={(e)=>{dispatchCounter({type:"RESET"})}}>RESET</button>
      <button onClick={(e)=>{dispatchCounter({type:"FROZEN"})}}>Frozen {counter.frozen ? "on" : "off"}</button>
    </div>
  );
}

export default App;
