import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';

const firebaseConfig = {
    apiKey: "AIzaSyDIcpvv3ZyX9TA81tx287xwVXJiVLuwPZo",
    authDomain: "sql-demos-6d3c4.firebaseapp.com",
    databaseURL: "https://sql-demos-6d3c4-default-rtdb.firebaseio.com",
    projectId: "sql-demos-6d3c4",
    storageBucket: "sql-demos-6d3c4.appspot.com",
    messagingSenderId: "846230834827",
    appId: "1:846230834827:web:1eb85422d4458e031cce8e"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const auth=firebase.auth();
  const db=firebase.firestore();
  const storage=firebase.storage();

  export {auth,firebase,db,storage}