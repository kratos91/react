import React, { useEffect } from 'react';
import {withRouter} from 'react-router-dom';

import {useDispatch,useSelector} from 'react-redux';
import {ingresoUsuarioAccion} from '../redux/usuarioDucks';

const Login = (props) => {
    const dispatch=useDispatch();

    const loading = useSelector(state => state.usuario.loading);
    const activo = useSelector(state => state.usuario.activo);

    useEffect(() => {
        if (activo) {
            props.history.push('/');
        }
    }, [activo,props.history]);

    return (
        <div className="mt-5 text-center">
            <h3>Ingreso con Google</h3>
            <hr/>
            <button 
                className="btn btn-dark"
                onClick={()=>dispatch(ingresoUsuarioAccion())}
                disabled={loading}
            >
                Acceder
            </button>
        </div>
    )
}

export default withRouter(Login);
