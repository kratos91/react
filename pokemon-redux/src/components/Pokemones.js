import React, { useEffect } from 'react';
import {useDispatch,useSelector} from 'react-redux';
import {obtenerPokemonesAccion,siguientePokemonAccion,anteriorPokemonAccion,unPokemonDetalleAccion} from '../redux/pokeDucks';
import Detalle from './Detalle';

const Pokemones = () => {
    
    const dispatch=useDispatch();
    
    const pokemones = useSelector(store=>store.pokemones.results);
    const next=useSelector(store=>store.pokemones.next);
    const previous=useSelector(store=>store.pokemones.previous);

    useEffect(() => {
        const fetchData=()=>{
            dispatch(obtenerPokemonesAccion());
        }

        fetchData();
    }, [dispatch])
    return (
        <div className="row">
            <div className="col-md-6">
                <h1>Lista de pokemones</h1>
                <hr/>
                <div className="d-flex justify-content-between">

                    {
                        pokemones.length===0 &&
                        <button onClick={()=>dispatch(obtenerPokemonesAccion())} className="btn btn-dark">Get Pokemones</button>
                    }

                    {
                        next &&
                        <button onClick={()=>dispatch(siguientePokemonAccion())} className="btn btn-dark">Siguientes Pokemones</button>
                    }

                    {
                        previous &&
                        <button onClick={()=>dispatch(anteriorPokemonAccion())} className="btn btn-dark">Anteriores Pokemones</button>
                    }
                </div>

                <ul className="list-group mt-3">
                    {
                        pokemones.map(item=>(
                            <li key={item.name} className="list-group-item tex-uppercase">
                                {item.name}
                                <button 
                                    className="btn btn-primary float-right"
                                    onClick={()=>dispatch(unPokemonDetalleAccion(item.url))}
                                >
                                    Detalles
                                </button>
                            </li>
                        ))
                    }
                </ul>
            </div>
            <div className="col-md-6">
                <h1>Detalle de pokemon</h1>
                <hr/>
                <Detalle/>
            </div>
        </div>
    )
}

export default Pokemones;
