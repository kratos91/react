import React from 'react';
import {Link,NavLink,withRouter} from 'react-router-dom';

import {useDispatch,useSelector} from 'react-redux';
import {cerrarSesionAccion} from '../redux/usuarioDucks';

const NavBar = (props) => {
    const dispath=useDispatch();

    const cerrarSesion=()=>{
        dispath(cerrarSesionAccion());
        props.history.push('login');
    }

    const activo = useSelector(state => state.usuario.activo);
    return (
        <div className="navbar navbar-dark bg-dark">
            <Link className="navbar-brand" to="/">APP POKE</Link>
            <div className="d-flex">
                {
                    activo ? (
                        <>
                            <NavLink className="btn btn-dark mr-2" to="/" exact>Inicio</NavLink>
                            <NavLink className="btn btn-dark mr-2" to="/perfil" exact>Perfil</NavLink>
                            <button className="btn btn-dark mr-2" onClick={()=>cerrarSesion()}>Cerrar Sesion</button>
                        </>
                    )
                    :(
                        <NavLink className="btn btn-dark mr-2" to="/login">Login</NavLink>
                    )
                }
            </div>
        </div>
    )
}

export default withRouter(NavBar);
