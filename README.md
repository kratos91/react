# Repositorio de ejemplos hechos con react

### Para instalar las dependencias:
npm install

### Para correr
npm start

* 1.Counter app
    - Ejemplos básicos de como usar los hooks principales
    - Pruebas unitarias y de integración básicas con enzyme
* 2.Gif-expert
    - Ejemplo de como hacer peticiones fecth a una api con custom hook
    - Animaciones sencillas con animated css
    - Test unitario y de implementacion
* 3.Hooks-app
    - Ejemplo de hook useState
    - Ejemplo de hook useEffect
    - Ejemplos de custom hooks
    - Ejemplo useFetch
    - Ejemplo useRef
    - Ejemplo useLayoutEffect
    - Ejemplo useMemo
    - Ejemplo useCallback
    - Ejemplo con useReducer
    - Ejemplo de useContext y Router

* 4.Heroes-app
    - Ejemplo de aplicacion con routers
    - Ejemplo con queryStrings
    - Uso de useContext
    - Protección de rutas

* 5.Journal-app
    - Ejemplo de aplicación con SASS
    - Ejemplo de autenticación y registro con firebase
    - Ejemplo de subida de imagenes a cloudinary
    - Ejemplo de Crud con firebase

* 6.Simple-crud
    - Ejemplo de un simple crud con useState
    - Ejemplo de crud con firebase
    - Ejemplo de autenticacion con firebase

* 7.Router-app
    - Ejemplo simple de llamadas a apis con useEffect

* 8.Pokemon-redux
    - Ejemplo de consumo de apis y uso de redux
    - Auth con firebase
    - Storage con firestore

* 9.Context-api
    - Ejemplo con context api en lugar de redux
     
#### Para solucionar problemas al iniciar react en linux

echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p