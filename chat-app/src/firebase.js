import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyDIcpvv3ZyX9TA81tx287xwVXJiVLuwPZo",
    authDomain: "sql-demos-6d3c4.firebaseapp.com",
    databaseURL: "https://sql-demos-6d3c4-default-rtdb.firebaseio.com",
    projectId: "sql-demos-6d3c4",
    storageBucket: "sql-demos-6d3c4.appspot.com",
    messagingSenderId: "846230834827",
    appId: "1:846230834827:web:1eb85422d4458e031cce8e"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const db=firebase.firestore();
  const auth=firebase.auth();
  const provider=new firebase.auth.GoogleAuthProvider();

  export {db,auth,provider};