import React from 'react';
import { 
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';
import { Contacto } from './components/Contacto';
import { Inicio } from './components/Inicio';
import { Nosotros } from './components/Nosotros';

function App() {
  return (
    <Router>
      <div className="container mt-5">
        <div className="container">
          <div className="btn-group">
            <Link to="/" className="btn btn-dark">Inicio</Link>
            <Link to="/contacto" className="btn btn-dark">Contacto</Link>
            <Link to="/nosotros" className="btn btn-dark">Nosotros</Link>
          </div>
        </div>
        <hr/>
        <Switch>
          <Route path="/nosotros/:id">
            <Nosotros/>
          </Route>
          <Route path="/nosotros">
            <Nosotros/>
          </Route>
          <Route path="/contacto">
            <Contacto/>
          </Route>
          <Route path="/" exact>
            <Inicio/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
