import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

export const Nosotros = () => {

    const {id}=useParams();
    const [user, setUser] = useState([]);

    useEffect(() => {
        const obtenerDatos=async()=>{
            const data=await fetch(`https://jsonplaceholder.typicode.com/users/${id}`);
            const usuario=await data.json();
            setUser(usuario);
        }

        obtenerDatos();
    }, [id]);

    

    return (
        <div>
            <h1>Nosotros</h1>
            <hr/>
            <h4>{user.name}</h4>
            <h4>{user.username}</h4>
            <h4>{user.email}</h4>
        </div>
    )
}
