import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

export const Inicio = () => {

    /* const datos=[
        {id:1,nombre:'ReactJs'},
        {id:2,nombre:'VueJs'},
        {id:3,nombre:'Angular'},
    ]; */

    const [count, setCount] = useState(0);
    const [equipo, setEquipo] = useState([]);

    useEffect(() => {
        document.title=`Has hecho click ${count}`;
    },[count]);

    useEffect(() => {
        obtenerDatos();
    }, []);

    const obtenerDatos=async()=>{
        const data=await fetch('https://jsonplaceholder.typicode.com/users');
        const users=await data.json();
        setEquipo(users);
    }

    return (
        <div>
            <h1>Inicio</h1>
            <button className="btn btn-primary" onClick={()=>{setCount(count+1)}}>Clickea</button>
            <hr/>
            <ul>
                {
                    equipo.map(item=>(
                        <li key={item.id}>
                            <Link to={`/nosotros/${item.id}`}>
                                {item.name}
                            </Link>
                        </li>
                    ))
                }
            </ul>
        </div>
    )
}
