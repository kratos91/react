import React , {useState,useEffect} from 'react';
import {withRouter} from 'react-router-dom';
import { db,auth } from '../firebase'

export const Inicio = (props) => {

  const [tarea, setTarea] = useState('');
  const [tareas, setTareas] = useState([]);
  const [modoEdicion, setModoEdicion] = useState(false);
  const [id, setId] = useState();

  const [user, setUser] = useState(null);

  let uid=localStorage.getItem("uid");
  
    useEffect(() => {
        if(auth.currentUser){
            setUser(auth.currentUser)
        }else{
            props.history.push('/login')
        }
    }, [props.history]);

  useEffect(() => {
    const obtnerDatos=async()=>{
      try {
        const data = await db.collection(uid).get();
        const arrayData = data.docs.map(doc=>({id:doc.id,...doc.data()}));
        setTareas(arrayData);
      } catch (error) {
        console.log(error);
      }
    }

    obtnerDatos();
  }, [uid]);

  const addTarea = async (e) => {
    e.preventDefault();
    
    if (!tarea.trim()) {
      console.log('Elemento vacio');
      return
    }
    
    try {
      const nuevaTarea={
        tarea,
        fecha:Date.now()
      }
      const data=await db.collection(uid).add(nuevaTarea);
      setTareas([
        ...tareas,
        {...nuevaTarea,id:data.id}
      ]);
      setTarea('');

    } catch (error) {
      console.log(error);
    }

  }

  const handleInputTarea=(e)=>{
    setTarea(e.target.value);
  }

  

  const handleDelete = async (id)=>{
    try {
      await db.collection(uid).doc(id).delete();
      setTareas(tareas.filter(item=>item.id !== id));
    } catch (error) {
      console.log(error);
    }
    
  }

  const handleUpdate=(item)=>{
    setModoEdicion(true);
    setTarea(item.tarea);
    setId(item.id);
  }

  const editarTarea= async (e)=>{
    e.preventDefault();
    
    if (!tarea.trim()) {
      return
    }
    
    try {
      await db.collection(uid).doc(id).update({tarea});

      setTareas(tareas.map(item=>item.id===id ? {id:item.id,fecha:item.fecha,tarea:item.fecha}: item));
  
      setTarea('');
      setModoEdicion(false);
    } catch (error) {
      console.log(error);
    }

  }

    return (
        <div className="container mt-5">
        <h1 className="text-center">Crud simple</h1>
        <hr/>
        <div className="row">
            <div className="col-8"> 
            <h4 className="text-center">Lista de tareas</h4>
            <ul className="list-group">
                {
                tareas.map(item=>(
                    <li className="list-group-item" key={item.id}>
                    <span className="lead">{item.tarea}</span>
                    <button 
                        className="btn btn-danger btn-sm float-right"
                        onClick={()=>handleDelete(item.id)}
                    > 
                        Eliminar
                    </button>
                    <button 
                        className="btn btn-warning btn-sm float-right mr-2"
                        onClick={()=>handleUpdate(item)}
                    >
                        Editar
                    </button>
                    </li>
                ))
                }
            </ul>
            </div>
            <div className="col-4"> 
            <h4 className="text-center">{modoEdicion ? 'Editar Tarea' : 'Agregar Tarea'}</h4>
            <form onSubmit={modoEdicion ? editarTarea : addTarea}>
                <input type="text" value={tarea} className="form-control mb-2" placeholder="Ingrese tarea" onChange={handleInputTarea}/>
                {
                modoEdicion ?
                (<button className="btn btn-warning btn-block" type="submit">Editar</button>)
                :
                (<button className="btn btn-dark btn-block" type="submit">Agregar</button>)
                }
            </form>
            </div>
        </div>
        </div>
    )
}

export default withRouter(Inicio);