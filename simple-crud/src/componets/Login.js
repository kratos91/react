import React, { useCallback, useState } from 'react';
import {withRouter} from 'react-router-dom';
import {auth,db} from '../firebase';

export const Login = (props) => {
    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');
    const [error, setError] = useState(null);
    const [esRegistro, setEsRegistro] = useState(true);

    const procesarDatos=(e)=>{
        e.preventDefault();
        if (!email.trim()) {
            setError('Ingrese Email');
            return
        }
        if (!pass.trim()) {
            setError('Ingrese Password');
            return
        }
        if (pass.length<6) {
            setError('Ingrese un Password de 6 caracteres');
            return
        }

        setError(null);
        if (esRegistro) {
            registrar();
        }else{
            login();
        }
    }

    const login=useCallback(async()=>{
        try {
            const res=await auth.signInWithEmailAndPassword(email,pass);

            localStorage.setItem("uid", res.user.uid);

            setEmail('');
            setPass('');
            setError(null);
            props.history.push('/admin');

        } catch (error) {
            setError(error.code);
        }
    },[email,pass,props.history]);

    const registrar=useCallback(async() => {
            try {
                const res=await auth.createUserWithEmailAndPassword(email,pass);
                await db.collection('usuarios').doc(res.user.email).set({
                    email:res.user.email,
                    uid:res.user.uid
                });
                
                await db.collection(res.user.uid).add({
                    name:'Primer tarea',
                    fecha:Date.now()
                });

                setEmail('');
                setPass('');
                setError(null);
                setEsRegistro(false);
            } catch (error) {
                setError(error.message);
            }
        },[email,pass]);

    return (
        <div className="mt-5">
            <h3 className="text-center">
                {
                    esRegistro ? 'Registro de usuarios' : 'Login de acceso'
                }
            </h3>
            <hr/>
            <div className="row justify-content-center">
                <div className="col-12 col-sm-8 col-md-6 col-xl-4">
                    <form onSubmit={procesarDatos}>
                        {
                            error && (
                                <div className="alert alert-danger">
                                    {error}
                                </div>
                            )
                        }
                        <input
                            type="text"
                            className="form-control mb-2"
                            placeholder="Ingrese un email"
                            onChange={e=>setEmail(e.target.value)}
                            value={email}
                        />
                        <input
                            type="password"
                            className="form-control mb-2"
                            placeholder="Ingrese un password"
                            autoComplete="off"
                            onChange={e=>setPass(e.target.value)}
                            value={pass}
                        />
                        <button 
                            type="submit"
                            className="btn btn-dark btn-lg col-12"
                        >
                            {
                                esRegistro ? 'Registrarse' : 'Acceder'
                            }
                        </button>
                        <button 
                            type="button"
                            className="btn btn-info btn-sm col-12 mt-2" 
                            onClick={()=>setEsRegistro(!esRegistro)}
                        >
                            {
                                esRegistro ? '¿Ya estas registrado?' : '¿No tienes cuenta?'
                            }
                        </button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default withRouter(Login);