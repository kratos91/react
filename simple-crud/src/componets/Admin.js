import React, { useEffect, useState } from 'react';
import {withRouter} from 'react-router-dom';
import {auth} from '../firebase';

export const Admin = (props) => {
    const [user, setUser] = useState(null);

    useEffect(() => {
        if(auth.currentUser){
            setUser(auth.currentUser)
        }else{
            props.history.push('/login')
        }
    }, [props.history]);

    return (
        <div>
            <h3 className="text-center">Ruta protegida</h3>
        </div>
    )
}

export default withRouter(Admin); 