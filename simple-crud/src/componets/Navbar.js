import React from 'react';
import {withRouter} from 'react-router-dom';
import {Link,NavLink} from 'react-router-dom';
import {auth} from '../firebase'

export const Navbar = (props) => {

    const cerrarSesion=()=>{
        auth.signOut()
            .then(()=>{
                props.history.push('/login');
            })
            .catch();
    }

    return (
        <div className="navbar navbar-dark bg-dark">
            <Link className="navbar-brand" to="/">AUTH</Link>
            <div>
                <div className="d-flex">
                    {
                        props.firebaseUser !==null ? (
                            <>
                                <NavLink className="btn btn-dark mr-2" to="/" exact>Inicio</NavLink>
                                <NavLink className="btn btn-dark mr-2" to="/admin">Admin</NavLink>
                            </>
                        ) : null
                    }
                    {
                        props.firebaseUser !== null ? (
                            <button className="btn btn-dark" onClick={()=>cerrarSesion()}>Cerrar sesión</button>
                        ) : (
                            <NavLink className="btn btn-dark mr-2" to="/login">Login</NavLink>
                        )
                    }
                </div>
            </div>
        </div>
    )
}

export default withRouter(Navbar);  