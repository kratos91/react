import React, { useEffect, useState } from 'react';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom';
import  Admin  from './componets/Admin';

import  Inicio  from './componets/Inicio';
import Login  from './componets/Login';
import Navbar  from './componets/Navbar';
import {auth} from './firebase';


function App() {  
  
  const [firebaseUser, setFirebaseUser] = useState(false);

  useEffect(() => {
    auth.onAuthStateChanged(user=>{
      if (user) {
        setFirebaseUser(user);
      } else {
        setFirebaseUser(null);
      }
    });  
  }, []);

  return firebaseUser !== false ? (
    <Router>
      <div className="container">
        <Navbar firebaseUser={firebaseUser}/>
        <Switch>
          <Route path="/login"><Login/></Route>
          <Route path="/admin"><Admin/></Route>
          <Route path="/" exact><Inicio/></Route>
        </Switch>
      </div>
    </Router> 
  ) : (
    <p>Loading...</p>
  );
}

export default App;
