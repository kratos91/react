import React from 'react';
import Navbar from './components/Navbar';
import ThemeContext from './context/ThemeProvider';
import HolaContext from './context/HolaProvider';

const App = () => {
    return (
        <ThemeContext>
            <Navbar/>
        </ThemeContext>
    )
}

export default App;
