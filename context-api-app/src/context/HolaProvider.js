import React, { createContext } from 'react';

export const HolaContext=createContext();

const HolaProvider = (props) => {
    
    const hola='Hola desde global';
    
    return (
        <HolaContext.Provider values={{hola}}>
            {props.child}
        </HolaContext.Provider>
    )
}

export default HolaProvider;
