import React, { useState } from 'react'
import PropTypes from 'prop-types';

export const AddCategory = ({setCategories}) => {
 
    const [inputValue, setInputValue] = useState('');

    const handleInputChange=(e)=>{
        //toma el valor del input del form y 
        //lo asigna a la variable 'inputValue'
        setInputValue(e.target.value);

        console.log('handleInputChange llamado')
    };

    const handleSubmit=(e)=>{
        //evita la recarga de la pagina
        e.preventDefault();

        if (inputValue.trim().length>2) {
            //toma la funcion setCategories que viene en los props
            //agarra las categorias y le agrega el inputValue
            setCategories(cats=>[inputValue,...cats]);
            //resetea el valor del input
            setInputValue('');
        }
        
    };

    return (
        <form onSubmit={handleSubmit}>
            <p>{inputValue}</p>
           <input 
                type="text"
                value={inputValue}
                onChange={handleInputChange}
           />
        </form>
    )
}
//Obliga a que sea pasado setCategories en los props
AddCategory.propTypes={
    setCategories:PropTypes.func.isRequired
}
