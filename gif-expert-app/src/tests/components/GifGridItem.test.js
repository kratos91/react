import React from 'react';
import { shallow } from "enzyme";
import { GifGridItem } from "../../components/GifGridItem";

describe('Pruebas en <GifGridItem/>', () => {

    const title='Un titulo';
    const url='http://localhost7algo.jpg';
 

    const wrapper=shallow(<GifGridItem title={title} url={url}/>);

    test('debe de mostrar el componente correctamente', () => {
        
        expect(wrapper).toMatchSnapshot();
    })
    
    test('debe de tener un parrafo con el titulo', () => {
        const p=wrapper.find('p');
        expect(p.text().trim()).toBe(title);
    })

    test('debe de tener la imagen url igual a la del props', () => {
        const img = wrapper.find('img');
        expect(img.prop('src')).toBe(url)
        expect(img.prop('alt')).toBe(title)
    })

    test('debe de tner la clase animate__bounce', () => {
        const div=wrapper.find('#primero');
        const className=div.prop('className');
        expect(className.includes('animate__bounce')).toBe(true)
    })
    
    
    
})
