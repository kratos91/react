import { types } from "../types/types";

/*
    state{
        uid:'asdfasfasfsafd',
        name:'Emiliano'
    }
*/
export const authReducer=(state={},action)=>{
    switch (action.type) {
        case types.login:
            return{
                uid:action.payload.uid,
                name:action.payload.displayName
            }
        case types.logout:
            return{}
        default:
            return state;
    }
}