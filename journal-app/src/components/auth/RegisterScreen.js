import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import validator from 'validator'
import { startRegisterWithEmailPasswordName } from '../../actions/auth';
import { removeError, setError } from '../../actions/ui';

import { useForm } from '../../hooks/useForm';

export const RegisterScreen = () => {

    const dispatch = useDispatch();
    //obtiene del redux valores
    const {msgError} = useSelector(state=>state.ui);

    const [formValues,handleInputChange]=useForm({
        name:'',
        email:'',
        password:'',
        password2:''
    });

    const {name,email,password,password2}=formValues;

    const handleRegister=(e)=>{
        e.preventDefault();
        if (isFormValue()) {
            dispatch(startRegisterWithEmailPasswordName(email,password,name));   
        }
        //dispatch(startLoginEmailPassword(email,password));    
    }

    const isFormValue=()=>{
        if (name.trim().length===0) {
            dispatch(setError('Name is required'));
            return false;
        }else if(!validator.isEmail(email)){
            dispatch(setError('Email is not valid'));
            return false;
        }else if(password !== password2 || password.length < 5){
            dispatch(setError('Password incorrect'));
            return false;
        }else{
            dispatch(removeError());
            return true;
        }
    }

    return (
        <>
            <h3 className="auth__title">Register</h3>
            <form onSubmit={handleRegister} className="animate__animated animate__fadeIn animate__delay-0.5s">
                {
                    msgError &&
                    (
                        <div className="auth__alert-error">
                            {msgError}
                        </div>
                    )
                }
                <input className="auth__input" type="text" placeholder="Name" name="name" onChange={handleInputChange}/>
                <input className="auth__input" type="text" placeholder="Email" name="email" autoComplete="off" onChange={handleInputChange}/>
                <input className="auth__input" type="password" placeholder="Password" name="password" onChange={handleInputChange}/>
                <input className="auth__input" type="password" placeholder="Confirm password" name="password2" onChange={handleInputChange}/>
                <button className="btn btn-primary btn-block mb-5" type="submit">Register</button>                
                <Link className="link" to="/auth/login">Already register?</Link>
            </form>
        </>
    )
}
