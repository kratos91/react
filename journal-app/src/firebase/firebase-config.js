import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyBOQmERGDuYFzDiQc1bOIzSANfGhwI9_Zk",
    authDomain: "react-journal-app-2ff23.firebaseapp.com",
    projectId: "react-journal-app-2ff23",
    storageBucket: "react-journal-app-2ff23.appspot.com",
    messagingSenderId: "975466621011",
    appId: "1:975466621011:web:b5b92800bddf0a4abd7ea6"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const db = firebase.firestore();
  const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

  export {
      db,
      googleAuthProvider,
      firebase
  }